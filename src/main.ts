import './assets/i18n';

import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { bootloader } from '@angularclass/hmr';

import { IsProduction } from './environments/environment';
import { AppModule } from './app/app.module';

function main() {
  if (IsProduction) {
    enableProdMode();
  }

  return platformBrowserDynamic().bootstrapModule(AppModule);
}

bootloader(main);
