import { ComponentFixture } from '@angular/core/testing';

export class FixtureControl {
  constructor(private fixture: ComponentFixture<any>) { }

  setInputValue(selector: string, inputValue: any) {
    let element = this.fixture.nativeElement.querySelector(selector);
    element.value = inputValue;
    element.dispatchEvent(new Event('input'));
  }

  click(selector: string) {
    let element = this.fixture.nativeElement.querySelector(selector);
    element.dispatchEvent(new Event('click'));
  }
}
