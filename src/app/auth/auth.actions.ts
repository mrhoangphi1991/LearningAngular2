import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';

@Injectable()
export class AuthActions {
  static AUTH = '[AUTH] Login';
  static AUTH_COMPLETED = '[AUTH] Login Completed';
  static AUTH_ERROR = '[AUT] Login Failure';
  static AUTH_LOGOUT = '[AUTH] Logout';

  auth(userName: string, password: string, navigateTo: any): Action {
    return {
      type: AuthActions.AUTH,
      payload: { userName, password, navigateTo }
    };
  }

  authCompleted(userName: string, navigateTo: any, response: any): Action {
    return {
      type: AuthActions.AUTH_COMPLETED,
      payload: { userName, navigateTo, token: response.json().access_token }
    };
  }

  authError(error: any): Action {
    return {
      type: AuthActions.AUTH_ERROR,
      payload: { errorMessage: error.json().error_description || 'Invalid Credentials' }
    };
  }

  authLogout(navigateTo: any): Action {
    return {
      type: AuthActions.AUTH_LOGOUT,
      payload: { navigateTo }
    };
  }
}
