import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ButtonModule } from 'primeng/components/button/button';
import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { MessagesModule } from 'primeng/components/messages/messages';

import { AuthRouting } from './auth.routing';

import { LoginComponent } from './login.component';
import { LogoutComponent } from './logout.component';

import { LoginService } from './login.service';
import { AuthActions } from './auth.actions';

@NgModule({
  imports: [CommonModule, FormsModule, ButtonModule, InputTextModule, MessagesModule, AuthRouting],
  declarations: [LoginComponent, LogoutComponent],
  providers: [LoginService, AuthActions],
})
export class AuthModule { }
