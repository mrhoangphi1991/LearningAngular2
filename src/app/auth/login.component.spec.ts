import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { FixtureControl } from '../testing/test.support';
import { MockLoginService } from './test.support';

import { reducer } from '../app.state';
import { AuthModule } from './auth.module';
import { AuthEffects } from './auth.effect';
import { LoginService } from './login.service';
import { LoginComponent } from './login.component';


describe('LoginComponent', () => {
  let spy = jasmine.createSpy('navigate');
  let router = { navigate: spy };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AuthModule, StoreModule.provideStore(reducer), EffectsModule.run(AuthEffects)],
      providers: [
        { provide: LoginService, useValue: new MockLoginService() },
        { provide: Router, useValue: router}
      ]
    });
  });

  beforeEach(() => {
    localStorage.clear();
    spy.calls.reset();
  });

  it('should redirect to contractors when login is successful', () => {
    let fixture = TestBed.createComponent(LoginComponent);
    let control = new FixtureControl(fixture);

    fixture.whenStable().then(() => {
      fixture.detectChanges();

      control.setInputValue('input[name="userName"]', 'user1');
      control.setInputValue('input[name="password"]', 'my-secret-password');
      control.click('button');

      expect(router.navigate).toHaveBeenCalledWith(['/contractors']);
    });
  });

  it('should display warning when login fails', () => {
    let fixture = TestBed.createComponent(LoginComponent);
    let control = new FixtureControl(fixture);

    fixture.whenStable().then(() => {
      fixture.detectChanges();

      control.setInputValue('input[name="userName"]', 'user1');
      control.setInputValue('input[name="password"]', 'wrong-password');
      control.click('button');

      fixture.detectChanges();

      let element = fixture.nativeElement.querySelector('.ui-messages-detail');
      expect(element.firstChild.nodeValue).toEqual('Invalid Credentials');
    })
  });

  it('must not redirect anywhere when login fails', () => {
    let fixture = TestBed.createComponent(LoginComponent);
    let control = new FixtureControl(fixture);

    fixture.whenStable().then(() => {
      fixture.detectChanges();

      control.setInputValue('input[name="userName"]', 'user1');
      control.setInputValue('input[name="password"]', 'wrong-password');
      control.click('button');

      expect(router.navigate).not.toHaveBeenCalled();
    })
  });
});
