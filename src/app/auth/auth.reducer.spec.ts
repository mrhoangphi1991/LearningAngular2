import { ResponseOptions, Response } from '@angular/http';

import { AuthActions } from './auth.actions';
import { AuthState, authReducer, initialState } from './auth.reducer';

describe('AuthState', () => {
  it('can create default instance', () => {
    const testee = AuthState.default;

    expect(testee.loggedIn).toBe(false);
    expect(testee.userName).toEqual('NOT LOGGED IN');
    expect(testee.token).toEqual('');
    expect(testee.errorMessage).toEqual('');
    expect(testee.hasError).toBe(false);
  });

  it('can create logged-in instance', () => {
    const testee = AuthState.loggedIn('user1', 'fake-jwt-token');

    expect(testee.loggedIn).toBe(true);
    expect(testee.userName).toEqual('user1');
    expect(testee.token).toEqual('fake-jwt-token');
    expect(testee.errorMessage).toEqual('');
    expect(testee.hasError).toBe(false);
  });

  it('can create instance with error message', () => {
    const testee = AuthState.withError('something bad has just happened');

    expect(testee.loggedIn).toBe(false);
    expect(testee.userName).toEqual('NOT LOGGED IN');
    expect(testee.token).toEqual('');
    expect(testee.errorMessage).toEqual('something bad has just happened');
    expect(testee.hasError).toBe(true);
  });
});

describe('AuthState (Initial State)', () => {
  it('should return default state when local storage is empty', () => {
    localStorage.clear();

    const state = initialState();

    expect(state).toEqual(AuthState.default);
  });

  it('should return logged-in state when local storage provides user name and token', () => {
    localStorage.setItem('userName', 'user1');
    localStorage.setItem('token', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ');

    const state = initialState();

    expect(state).toEqual(AuthState.loggedIn('user1', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ'));
  })
});

describe('AuthReducer', () => {
  let actions: AuthActions;

  beforeEach(() => {
    actions = new AuthActions();
  });

  it('should return current state when no valid action has been made', () => {
    const state = AuthState.default;
    const actual = authReducer(state, { type: 'INVALID_ACTION', payload: {} });
    const expected = state;

    expect(actual).toEqual(expected);
  });

  it('should return default state when authentication starts', () => {
    const state = AuthState.loggedIn('user1', 'fake-jwt-token');
    const actual = authReducer(state, actions.auth('user2', 'my-secret-password', ['/my-route']));

    expect(actual).toEqual(AuthState.default);
  });

  it('should return logged-in state when authentication completes', () => {
    const response = new Response(
      new ResponseOptions({
        body: JSON.stringify({ access_token: 'fake-jwt-token' })
      }));

    const state = AuthState.default;
    const actual = authReducer(state, actions.authCompleted('user2', ['/my-route'], response));

    expect(actual).toEqual(AuthState.loggedIn('user2', 'fake-jwt-token'));
  });

  it('should return error state when authentication fails', () => {
    const response = new Response(
      new ResponseOptions({
        body: JSON.stringify({ error_description: 'something bad has just happened' })
      }));

    const state = AuthState.default;
    const actual = authReducer(state, actions.authError(response));

    expect(actual).toEqual(AuthState.withError('something bad has just happened'));
  });

  it('should return error state when authentication fails and no error description is provided', () => {
    const response = new Response(
      new ResponseOptions({
        body: JSON.stringify({})
      }));

    const state = AuthState.default;
    const actual = authReducer(state, actions.authError(response));

    expect(actual).toEqual(AuthState.withError('Invalid Credentials'));
  });

  it('should return default state when logging out', () => {
    const state = AuthState.loggedIn('user1', 'fake-jwt-token');
    const actual = authReducer(state, actions.authLogout(['/home']));

    expect(actual).toEqual(AuthState.default);
  });
});
