import { TestBed, inject } from '@angular/core/testing';
import { Router } from '@angular/router';
import { EffectsTestingModule, EffectsRunner } from '@ngrx/effects/testing';

import { MockLoginService } from './test.support';

import { AuthActions } from './auth.actions';
import { AuthEffects } from './auth.effect';
import { LoginService } from './login.service';

describe('AuthEffects', () => {
  let loginService = new MockLoginService();
  let router = { navigate: jasmine.createSpy('navigate') };

  beforeEach(() => TestBed.configureTestingModule({
    imports: [EffectsTestingModule],
    providers: [
      AuthEffects,
      AuthActions,
      { provide: LoginService, useValue: loginService },
      { provide: Router, useValue: router }
    ]
  }));

  let runner: EffectsRunner;
  let authEffects: AuthEffects;

  beforeEach(inject([EffectsRunner, AuthEffects], (_runner, _authEffects) => {
      runner = _runner;
      authEffects = _authEffects;
    }
  ));

  let actions: AuthActions;

  beforeEach(() => {
    actions = new AuthActions();
  });

  it('should return an AUTH_COMPLETED action after successful authentication', () => {
    runner.queue(actions.auth('user1', 'my-secret-password', ['/my-route']));

    authEffects.login$.subscribe(result => {
      expect(result).toEqual({
        type: AuthActions.AUTH_COMPLETED,
        payload: { userName: 'user1', navigateTo: ['/my-route'], token: 'fake-jwt-token' }
      });
    });
  });

  it('should store user name and access token into local storage after successful authentication', () => {
    runner.queue(actions.authCompleted('user1', ['/my-route'], loginService.Response));

    authEffects.loginCompleted$.subscribe(() => {
      expect(localStorage.getItem('userName')).toEqual('user1');
      expect(localStorage.getItem('token')).toEqual('fake-jwt-token');
    });

    localStorage.clear();
  });

  it('should navigate to given route after successful authentication', () => {
    runner.queue(actions.authCompleted('user1', ['/my-route'], loginService.Response));

    authEffects.loginCompleted$.subscribe(() => {
      expect(router.navigate).toHaveBeenCalledWith(['/my-route']);
    });
  });

  it('should return an AUTH_ERROR action after failed authentication', () => {
    runner.queue(actions.auth('user1', 'wrong-password', ['/my-route']));

    authEffects.login$.subscribe(result => {
      expect(result).toEqual({
        type: AuthActions.AUTH_ERROR,
        payload: { errorMessage: 'Invalid Credentials' }
      });
    });
  });

  it('should clear local storage after logout', () => {
    localStorage.setItem('userName', 'user1');
    localStorage.setItem('token', 'fake-jwt-token');

    runner.queue(actions.authLogout(['/home']));

    authEffects.logout$.subscribe(() => {
      expect(localStorage.length).toBe(0);
    });
  });

  it('should navigate to given route after logout', () => {
    runner.queue(actions.authLogout(['/home']));

    authEffects.logout$.subscribe(() => {
      expect(router.navigate).toHaveBeenCalledWith(['/home']);
    });
  });
});
