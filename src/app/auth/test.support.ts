import { Observable } from 'rxjs';
import { ResponseOptions, Response } from '@angular/http';

export class MockLoginService {
  private successfulResponse = new Response(
    new ResponseOptions({
      body: JSON.stringify({ access_token: 'fake-jwt-token' })
    }));

  private failingResponse = new Response(
    new ResponseOptions({
      body: JSON.stringify({ })
    }));

  get Response() {
    return this.successfulResponse;
  }

  login(username: string, password: string) {
    if (password === 'my-secret-password') {
      return Observable.of(this.successfulResponse);
    }

    return Observable.throw(this.failingResponse);
  }
}
