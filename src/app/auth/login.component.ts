import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Message } from 'primeng/components/common/api';

import { AppState } from '../app.state';
import { AuthActions } from './auth.actions';
import { AuthState } from './auth.reducer';

@Component({
  selector: 'p17-login',
  templateUrl: 'login.component.html'
})
export class LoginComponent {
  username: string;
  password: string;
  messages: Message[] = [];

  constructor(private store: Store<AppState>, private actions: AuthActions) {
    this.store.select(s => s.auth).subscribe(auth => this.showWarning(auth));
  }

  login() {
    this.store.dispatch(this.actions.auth(this.username, this.password, ['/contractors']));
  }

  showWarning(auth: AuthState) {
    if (auth.hasError) {
      this.messages.push({ severity: 'warn', detail: auth.errorMessage });
    }
  }
}
