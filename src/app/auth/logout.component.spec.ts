import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { MockLoginService } from './test.support';

import { reducer } from '../app.state';
import { LogoutComponent } from './logout.component';
import { LoginService } from './login.service';
import { AuthEffects } from './auth.effect';
import { AuthModule } from './auth.module';

describe('LogoutComponent', () => {
  let router = { navigate: jasmine.createSpy('navigate') };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AuthModule, StoreModule.provideStore(reducer), EffectsModule.run(AuthEffects)],
      providers: [
        { provide: LoginService, useValue: new MockLoginService() },
        { provide: Router, useValue: router}
      ]
    });
  });

  it('should redirect to home when logging out', () => {
    let fixture = TestBed.createComponent(LogoutComponent);

    fixture.whenStable().then(() => {
      fixture.detectChanges();

      expect(router.navigate).toHaveBeenCalledWith(['/home']);
    })
  });
});
