import { Injectable, Inject } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class LoginService {

  constructor(
    private http: Http,
    @Inject('baseUrl') private baseUrl: string) { }

  login(username: string, password: string) {
    let url = this.baseUrl + '/oauth/token';
    let body = 'username=' + username + '&password=' + password + '&grant_type=password';
    let headerOptions = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };

    let headers = new Headers(headerOptions);
    let options = new RequestOptions({ headers: headers });

    return this.http.post(url, body, options);
  }
}
