import { Action } from '@ngrx/store';
import { tokenNotExpired } from 'angular2-jwt';

import { AuthActions } from './auth.actions';

export class AuthState {
  loggedIn: boolean;
  userName: string;
  token: string;
  errorMessage: string;

  private constructor(loggedIn: boolean, userName: string, token: string, errorMessage: string) {
    this.loggedIn = loggedIn;
    this.userName = userName;
    this.token = token;
    this.errorMessage = errorMessage;
  }

  static get default(): AuthState {
    return new AuthState(false, 'NOT LOGGED IN', '', '');
  }

  static loggedIn(userName: string, token: string): AuthState {
    return new AuthState(true, userName, token, '');
  }

  static withError(errorMessage): AuthState {
    return new AuthState(false, 'NOT LOGGED IN', '', errorMessage);
  }

  get hasError(): boolean {
    return this.errorMessage !== '';
  }
}

export function initialState(): AuthState {
  let userName = localStorage.getItem('userName');
  let token = localStorage.getItem('token');

  if ((userName !== null && token !== null) && tokenNotExpired(null, token)) {
      return AuthState.loggedIn(userName, token);
  }

  return AuthState.default;
}

export function authReducer(state = initialState(), action: Action): AuthState {
  switch (action.type) {
    case AuthActions.AUTH:
      return AuthState.default;

    case AuthActions.AUTH_COMPLETED:
      return AuthState.loggedIn(action.payload.userName, action.payload.token);

    case AuthActions.AUTH_ERROR:
      return AuthState.withError(action.payload.errorMessage);

    case AuthActions.AUTH_LOGOUT:
      return AuthState.default;

    default: return state;
  }
}
