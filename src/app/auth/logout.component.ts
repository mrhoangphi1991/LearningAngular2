import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState } from '../app.state';
import { AuthActions } from './auth.actions';

@Component({
  selector: 'p17-logout',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LogoutComponent implements OnInit {
  constructor(
    private store: Store<AppState>,
    private actions: AuthActions) { }

  ngOnInit() {
    this.store.dispatch(this.actions.authLogout(['/home']));
  }
}
