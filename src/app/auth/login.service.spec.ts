import { BaseRequestOptions, Http, Response, ResponseOptions } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { TestBed, inject } from '@angular/core/testing';

import { LoginService } from './login.service';

const mockHttpProvider = {
  provide: Http,
  useFactory: (backend: MockBackend, defaultOptions: BaseRequestOptions) => {
    return new Http(backend, defaultOptions);
  },
  deps: [MockBackend, BaseRequestOptions]
};

const testEnvironmentProvider = { provide: 'baseUrl', useValue: 'http://localhost:6161' };

describe('LoginService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        LoginService,
        MockBackend,
        BaseRequestOptions,
        mockHttpProvider,
        testEnvironmentProvider
      ]
    });
  });

  it('should get access token when posting with credentials to token endpoint',
    inject([MockBackend, LoginService], (backend: MockBackend, loginService: LoginService) => {
      let response = new Response(
        new ResponseOptions({
          body: JSON.stringify({ access_token: 'fake-jwt-token' })
        }));

      backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

      loginService.login('user1', 'my-secret-password').subscribe(
        response => expect(response.json().access_token).toEqual('fake-jwt-token'));
    }));

  it('should report error when posting with credentials fails',
    inject([MockBackend, LoginService], (backend: MockBackend, loginService: LoginService) => {
      backend.connections.subscribe((c: MockConnection) => c.mockError(new Error('Something bad happened')));

      loginService.login('user1', 'my-secret-password').subscribe(
        () => { },
        error => { expect(error.message).toEqual('Something bad happened'); });
    }));
});
