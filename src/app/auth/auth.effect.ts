import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

import { AuthActions } from './auth.actions';
import { LoginService } from './login.service';

@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions,
    private authActions: AuthActions,
    private router: Router,
    private loginService: LoginService) { }

  @Effect() login$ = this.actions$
    .ofType(AuthActions.AUTH)
    .map(action => action.payload)
    .switchMap((payload) => this.loginService.login(payload.userName, payload.password)
      .map((response) => this.authActions.authCompleted(payload.userName, payload.navigateTo, response))
      .catch((error) => Observable.of(this.authActions.authError(error)))
    );

  @Effect() loginCompleted$ = this.actions$
    .ofType(AuthActions.AUTH_COMPLETED)
    .map(action => action.payload)
    .do(payload => {
      localStorage.setItem('userName', payload.userName);
      localStorage.setItem('token', payload.token);
      this.router.navigate(payload.navigateTo);
    });

  @Effect() logout$ = this.actions$
    .ofType(AuthActions.AUTH_LOGOUT)
    .map(action => action.payload)
    .do(payload => {
      localStorage.clear();
      this.router.navigate(payload.navigateTo);
    });
}
