import { NgModule, ApplicationRef, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Http } from '@angular/http';
import { StoreModule, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { removeNgStyles, createNewHosts, createInputTransfer } from '@angularclass/hmr';
import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate';

import 'rxjs/add/operator/take';

import { CoreModule } from './core/core.module';
import { AuthModule } from './auth/auth.module';
import { HomeModule } from './+home/home.module';
import { ContractorsModule } from './+contractors/contractors.module';
import { PlaygroundModule } from './+playground/playground.module';

import { AppState, reducer } from './app.state';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header.component';
import { NavigationComponent } from './navigation.component';
import { ProfileComponent } from './profile.component';

import { ComponentsModule } from './components/components.module';
import { AppRoutingModule } from './app.routing';
import { AuthEffects } from './auth/auth.effect';
import { ENV_PROVIDERS } from '../environments/environment';

export function createTranslateLoader(http: Http) {
  return new TranslateStaticLoader(http, './assets/i18n', '.json');
}

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    StoreModule.provideStore(reducer),
    EffectsModule.run(AuthEffects),
    TranslateModule.forRoot({ provide: TranslateLoader, useFactory: (createTranslateLoader), deps: [Http] }),
    CoreModule.forRoot(),
    ComponentsModule,
    AuthModule,
    HomeModule,
    ContractorsModule,
    PlaygroundModule,
    AppRoutingModule],
  declarations: [AppComponent, HeaderComponent, NavigationComponent, ProfileComponent],
  providers: [ENV_PROVIDERS],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
  constructor(public appRef: ApplicationRef, public store: Store<AppState>) {
  }

  hmrOnInit(store) {
    if (!store || !store.rootState) {
      return;
    }

    if (store.rootState) {
      this.store.dispatch({
        type: 'SET_ROOT_STATE',
        payload: store.rootState
      });
    }

    if ('restoreInputValues' in store) {
      store.restoreInputValues();
    }
    this.appRef.tick();
    Object.keys(store).forEach(prop => delete store[prop]);
  }

  hmrOnDestroy(store) {
    const cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
    this.store.take(1).subscribe(s => store.rootState = s);
    store.disposeOldHosts = createNewHosts(cmpLocation);
    store.restoreInputValues = createInputTransfer();
    removeNgStyles();
  }

  hmrAfterDestroy(store) {
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }
}
