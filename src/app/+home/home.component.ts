import { Component } from '@angular/core';
import { TranslateService } from 'ng2-translate';

@Component({
  selector: 'p17-home',
  template: `<h2>{{ 'HOME.TITLE' | translate }}</h2>`
})
export class HomeComponent {
  constructor(private translate: TranslateService) { }
}
