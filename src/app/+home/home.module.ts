import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from 'ng2-translate';

import { HomeRouting } from './home.routing';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [CommonModule, TranslateModule, HomeRouting],
  declarations: [HomeComponent]
})
export class HomeModule { }
