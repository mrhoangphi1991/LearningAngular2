import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { AppState } from './app.state';

@Component({
  selector: 'p17-profile',
  templateUrl: 'profile.component.html'
})
export class ProfileComponent {
  userName: string;
  loggedIn: boolean;
  subMenuActive: boolean = false;

  constructor(private store: Store<AppState>, private router: Router) {
    this.store.select(s => s.auth).subscribe(auth => {
      this.userName = auth.userName;
      this.loggedIn = auth.loggedIn;
    });
  }

  showSubMenu() {
    this.subMenuActive = true;
  }

  hideSubMenu() {
    this.subMenuActive = false;
  }

  itemClick(route: any[]) {
    if (route) {
      this.router.navigate(route);
      this.hideSubMenu();
    }
  }
}
