import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/components/common/api';

@Component({
  selector: 'p17-navigation',
  template: `<p17-megamenu [model]="items"></p17-megamenu>`
})
export class NavigationComponent implements OnInit {
  items: MenuItem[];

  ngOnInit() {
    this.items = [
      { label: 'Home', icon: 'fa-home', routerLink: ['/home'] },
      { label: 'Contractors', icon: 'fa-user-md', routerLink: ['/contractors'] },
      { label: 'Playground', icon: 'fa-flask', routerLink: ['/playground'] }
    ];
  }
}
