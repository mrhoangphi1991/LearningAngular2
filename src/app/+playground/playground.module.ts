import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaygroundRouting } from './playground.routing';
import { PlaygroundComponent } from './playground.component';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { DialogModule } from 'primeng/components/dialog/dialog';

@NgModule({
  imports: [CommonModule, DataTableModule, DialogModule, PlaygroundRouting],
  declarations: [PlaygroundComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PlaygroundModule { }
