import { RouterModule } from '@angular/router';

import { PlaygroundComponent } from './playground.component';

export const PlaygroundRouting = RouterModule.forChild([
  { path: 'playground', component: PlaygroundComponent }
]);
