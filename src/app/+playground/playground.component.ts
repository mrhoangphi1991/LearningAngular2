import { Component } from '@angular/core';

@Component({
  selector: 'p17-playground',
  templateUrl: 'playground.component.html'
})
export class PlaygroundComponent {
  contextMenuVisible: boolean = false;

  toggleContextMenu() {
    this.contextMenuVisible = !this.contextMenuVisible;
  }

  showContextMenu() {
    this.contextMenuVisible = true;
  }

  hideContextMenu() {
    this.contextMenuVisible = false;
  }
}
