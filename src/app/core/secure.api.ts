import { Injectable, Inject } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { AppState } from '../app.state';

@Injectable()
export class SecureApi {
  private token: string;

  constructor(
    private store: Store<AppState>,
    private http: Http,
    @Inject('baseUrl') private baseUrl: string) {
    this.store.select(s => s.auth).subscribe(auth => this.token = auth.token);
  }

  get(resource: string) {
    let url = this.baseUrl + resource;
    let headerOptions = {
      'Accept': 'application/json',
      'Access-Control-Allow-Credentials': 'true',
      'Authorization': 'Bearer ' + this.token
    };

    let headers = new Headers(headerOptions);
    let options = new RequestOptions({ headers: headers });

    return this.http.get(url, options)
      .map(response => response.json())
      .catch(this.handleError);
  }

  handleError(error: any) {
    console.warn(error);
    return Observable.throw(error.json().message || 'Server error');
  }
}
