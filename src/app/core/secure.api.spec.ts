import { BaseRequestOptions, Http, RequestMethod, Response, ResponseOptions } from '@angular/http';
import { TestBed, inject } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { StoreModule, Store } from '@ngrx/store';

import { reducer, AppState } from '../app.state';
import { AuthActions } from '../auth/auth.actions';
import { SecureApi } from './secure.api';

const mockHttpProvider = {
  provide: Http,
  useFactory: (backend: MockBackend, defaultOptions: BaseRequestOptions) => {
    backend.connections.subscribe((connection: MockConnection) => {
      if (connection.request.url.endsWith('api/resource') && (connection.request.method === RequestMethod.Get)) {
        if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
          connection.mockRespond(new Response(
            new ResponseOptions({ status: 200, body: JSON.stringify({ resource: 'my-resource' }) })
          ));
        } else {
          connection.mockRespond(new Response(
            new ResponseOptions({ status: 401, body: JSON.stringify({ message: 'forbidden' }) })
          ));
        }
      }
    });

    return new Http(backend, defaultOptions);
  },
  deps: [MockBackend, BaseRequestOptions]
};

const testEnvironmentProvider = { provide: 'baseUrl', useValue: 'http://localhost:6161' };

describe('SecureApi', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.provideStore(reducer)],
      providers: [
        SecureApi,
        MockBackend,
        BaseRequestOptions,
        mockHttpProvider,
        testEnvironmentProvider
      ]
    })
  });

  it('get should return response if request has auth header with valid JWT token',
    inject([Store, SecureApi], (store: Store<AppState>, api: SecureApi) => {
      store.dispatch({ type: AuthActions.AUTH_COMPLETED, payload: { token: 'fake-jwt-token' } });

      api.get('api/resource').subscribe(
        response => expect(response.resource).toEqual('my-resource'));
    }));

  it('get should fail if request has auth header with invalid JWT token',
    inject([Store, SecureApi], (store: Store<AppState>, api: SecureApi) => {
      store.dispatch({ type: AuthActions.AUTH_COMPLETED, payload: { token: 'invalid-jwt-token' } });

      api.get('api/resource').subscribe(
        () => { },
        error => expect(error.message).toEqual('forbidden'));
    }));

  it('get should fail if request has no auth header', inject([SecureApi], (api: SecureApi) => {
    api.get('api/resource').subscribe(
      () => { },
      error => expect(error.message).toEqual('forbidden'));
  }));
});
