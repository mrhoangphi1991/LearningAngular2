import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';

import { AppState } from '../app.state';

@Injectable()
export class AuthGuard implements CanActivate {
  private loggedIn: boolean;

  constructor(
    private store: Store<AppState>,
    private router: Router) {
    this.store.select(s => s.auth).subscribe(auth => this.loggedIn = auth.loggedIn);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // return true;
    if (this.loggedIn) {
      return true;
    }

    this.router.navigate(['/login']);
    return false;
  }
}
