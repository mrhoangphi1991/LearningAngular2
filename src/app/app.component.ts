import { Component, Inject } from '@angular/core';
import { TranslateService } from 'ng2-translate';

@Component({
  selector: 'p17-app',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(private translate: TranslateService, @Inject('defaultLanguage') private defaultLanguage: string) {
    this.translate.addLangs(['en', 'de']);
    this.translate.setDefaultLang(this.defaultLanguage);
  }
}
