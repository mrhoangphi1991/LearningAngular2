import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContractorsRouting } from './contractors.routing';
import { ContractorsComponent } from './contractors.component';

@NgModule({
  imports: [CommonModule, ContractorsRouting],
  declarations: [ContractorsComponent]
})
export class ContractorsModule { }
