import { RouterModule } from '@angular/router';

import { AuthGuard } from '../core/auth.guard';
import { ContractorsComponent } from './contractors.component';

export const ContractorsRouting = RouterModule.forChild([
  { path: 'contractors', component: ContractorsComponent, canActivate: [AuthGuard] }
]);
