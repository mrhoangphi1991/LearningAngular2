import { Component, OnInit } from '@angular/core';
import { SecureApi } from '../core/secure.api';

export interface Contractor {
  firstName: string;
  lastName: string;
}

@Component({
  selector: 'p17-contractors',
  template: `
  <h2>Contractors</h2>
  <ul>
    <li *ngFor="let contractor of contractors">{{ contractor.firstName }} {{ contractor.lastName }}</li>
  </ul>`
})
export class ContractorsComponent implements OnInit {
  contractors: Contractor[];
  errorMessage: string;

  constructor(private secureApi: SecureApi) { }

  ngOnInit() {
    this.secureApi
      .get('/api/v1/contractors')
      .map(result => result.items)
      .subscribe(
        items => this.contractors = items,
        error => this.errorMessage = error
      );
  }
}
