import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MenuItem } from 'primeng/components/common/api';

@Component({
  selector: 'p17-megamenu',
  template: `
  <div class="ui-menu ui-menubar ui-megamenu ui-widget ui-widget-content ui-corner-all ui-helper-clearfix">
    <ul class="ui-menu-list ui-helper-reset ui-menubar-root-list">
      <li *ngFor="let category of model" class="ui-menuitem ui-widget ui-corner-all">
          <a class="ui-menuitem-link ui-corner-all ui-submenu-link" routerLinkActive="ui-menuitem-active" [routerLink]="category.routerLink">
             <span class="ui-menuitem-icon fa fa-fw" [ngClass]="category.icon"></span>
             {{ category.label }}
          </a>
      </li>
    </ul>
  </div>`
})
export class MegaMenuComponent {
  @Input() model: MenuItem[];
}
