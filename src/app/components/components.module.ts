import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MegaMenuComponent } from './megamenu.component';

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [MegaMenuComponent],
  exports: [MegaMenuComponent]
})
export class ComponentsModule { }
