import { Component } from '@angular/core';

@Component({
  selector: 'p17-header',
  template: `
  <div class="top-bar">
    <div class="navigation-bar">
      <p17-navigation></p17-navigation>
    </div>
    <div class="profile-settings">
      <p17-profile></p17-profile>
    </div>
  </div>`
})
export class HeaderComponent { }
