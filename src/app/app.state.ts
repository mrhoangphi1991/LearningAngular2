import { ActionReducer, combineReducers } from '@ngrx/store';
import { compose } from '@ngrx/core/compose';

import { IsProduction } from '../environments/environment';
import { AuthState, authReducer } from './auth/auth.reducer';

export interface AppState {
  auth: AuthState;
}

const reducers = {
  auth: authReducer
};

function stateSetter(reducer: ActionReducer<any>): ActionReducer<any> {
  return function (state, action) {
    if (action.type === 'SET_ROOT_STATE') {
      return action.payload;
    }

    return reducer(state, action);
  };
}

const developmentReducer: ActionReducer<AppState> = compose(stateSetter, combineReducers)(reducers);
const productionReducer: ActionReducer<AppState> = combineReducers(reducers);

export function reducer(state: any, action: any) {
  if (IsProduction) {
    return productionReducer(state, action);
  }

  return developmentReducer(state, action);
};
