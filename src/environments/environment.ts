let providers: any[] = [
  { provide: 'environment', useValue: 'Production' },
  { provide: 'version', useValue: '0.1.0' },
  { provide: 'baseUrl', useValue: 'http://localhost:6161' },
  { provide: 'defaultLanguage', useValue: 'de' }
];

export const ENV_PROVIDERS = providers;
export const IsProduction = true;
