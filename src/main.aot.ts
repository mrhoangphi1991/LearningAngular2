import './polyfills';
import './vendor';
import './assets/i18n';

import { enableProdMode } from '@angular/core';
import { platformBrowser } from '@angular/platform-browser';
import { AppModuleNgFactory } from '../aot/src/app/app.module.ngfactory';

function main() {
  enableProdMode();
  platformBrowser().bootstrapModuleFactory(AppModuleNgFactory);
}

function bootloader(main) {
  if (document.readyState === 'complete') {
    main();
  } else {
    document.addEventListener('DOMContentLoaded', main);
  }
}

bootloader(main);
